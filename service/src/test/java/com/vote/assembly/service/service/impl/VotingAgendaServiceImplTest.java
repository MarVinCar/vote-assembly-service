package com.vote.assembly.service.service.impl;

import com.vote.assembly.service.dto.VotingAgendaDto;
import com.vote.assembly.service.exception.NotFoundException;
import com.vote.assembly.service.exception.VotingAgendaNotFoundException;
import com.vote.assembly.service.model.VotingAgenda;
import com.vote.assembly.service.repository.VotingAgendaRepository;
import com.vote.assembly.service.util.Messages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static fixture.VotingAgendaFixture.buildVotingAgenda;
import static fixture.VotingAgendaFixture.buildVotingAgendaDto;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VotingAgendaServiceImplTest {

    @InjectMocks
    private VotingAgendaServiceImpl votingAgendaService;

    @Mock
    private VotingAgendaRepository votingAgendaRepository;

    @Mock
    private Messages message;

    @BeforeEach
    void setUp() {
    }

    @Nested
    @DisplayName("When the createVotingAgenda method is called")
    class WhenTheCreateVotingAgendaMethodIsCalled {

        @Nested
        @DisplayName("With invalid parameters")
        class WithInvalidParameters {

            @Test
            @DisplayName("Must point null value")
            void mustPointNullValue() {
                VotingAgendaDto dto = new VotingAgendaDto();

                VotingAgenda result = votingAgendaService.createVotingAgenda(dto);

                assertThat(result, nullValue());
            }
        }

        @Nested
        @DisplayName("With valid parameters")
        class WithValidParameters {

            @Test
            @DisplayName("Must create a voting agenda")
            void mustCreateAVotingAgenda() {
                VotingAgendaDto dto = buildVotingAgendaDto();
                VotingAgenda votingAgenda = buildVotingAgenda();

                votingAgendaService.createVotingAgenda(dto);

                verify(votingAgendaRepository).save(votingAgenda);
            }
        }
    }

    @Nested
    @DisplayName("When the findAll method is called")
    class WhenTheFindAllMethodIsCalled {

        @Nested
        @DisplayName("When there is no data in the database")
        class WhenThereIsNoDataInTheDatabase {

            @Test
            @DisplayName("Must return NotFoundException")
            void mustReturnNotFoundException() {
                List<VotingAgenda> votingAgendaList = new ArrayList<>();

                when(votingAgendaRepository.findAll())
                        .thenReturn((votingAgendaList));

                assertThrows(NotFoundException.class,
                        () -> votingAgendaService.findAll());
            }
        }

        @Nested
        @DisplayName("When there is data in the database")
        class WhenThereIsDataInTheDatabase {

            @Test
            @DisplayName("Should return a list of voting agenda")
            void shouldReturnAListOfVotingAgenda() {
                List<VotingAgenda> votingAgendaList = List.of(buildVotingAgenda());

                when(votingAgendaRepository.findAll())
                        .thenReturn((votingAgendaList));

                List<VotingAgenda> result = votingAgendaService.findAll();

                assertThat(result.get(0).getVotingAgendaTitle(), is("Title"));
            }
        }
    }

    @Nested
    @DisplayName("When the findVotingAgendaById method is called")
    class WhenTheFindVotingAgendaByIdMethodIsCalled {

        @Nested
        @DisplayName("When voting agenda is not found in the database")
        class WhenVotingAgendaIsNotFoundInTheDatabase {

            @Test
            @DisplayName("Must return VotingAgendaNotFoundException")
            void mustReturnVotingAgendaNotFoundException() {
                when(votingAgendaRepository.findVotingAgendaById(any()))
                        .thenThrow(VotingAgendaNotFoundException.class);

                assertThrows(VotingAgendaNotFoundException.class,
                        () -> votingAgendaService.findVotingAgendaById(1L));
            }
        }

        @Nested
        @DisplayName("When voting agenda is found in the database")
        class WhenVotingAgendaIsFoundInTheDatabase {

            @Test
            @DisplayName("Should return a voting agenda")
            void shouldReturnAVotingAgenda() {
                when(votingAgendaRepository.findVotingAgendaById(any()))
                        .thenReturn(buildVotingAgenda());

                VotingAgenda result = votingAgendaService.findVotingAgendaById(1L);

                assertThat(result.getVotingAgendaTitle(), is("Title"));
            }
        }
    }
}
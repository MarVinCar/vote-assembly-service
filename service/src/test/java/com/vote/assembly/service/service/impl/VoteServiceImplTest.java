package com.vote.assembly.service.service.impl;

import com.vote.assembly.service.dto.MemberVoteDto;
import com.vote.assembly.service.exception.NotFoundException;
import com.vote.assembly.service.exception.VotingAgendaNotFoundException;
import com.vote.assembly.service.feign.ClientValidateCpf;
import com.vote.assembly.service.model.MemberVote;
import com.vote.assembly.service.model.VotingAgenda;
import com.vote.assembly.service.response.VotingResponse;
import com.vote.assembly.service.repository.VoteRepository;
import com.vote.assembly.service.service.AssemblySessionService;
import com.vote.assembly.service.service.VotingAgendaService;
import com.vote.assembly.service.util.Messages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static fixture.AssemblySessionFixture.buildAssemblySession;
import static fixture.MemberVoteFixture.*;
import static fixture.VotingAgendaFixture.buildVotingAgendaWithId;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VoteServiceImplTest {

    @InjectMocks
    private VoteServiceImpl voteService;

    @Mock
    private AssemblySessionService assemblySessionService;

    @Mock
    private ClientValidateCpf client;

    @Mock
    private Messages message;

    @Mock
    private VoteRepository voteRepository;

    @Mock
    private VotingAgendaService votingAgendaService;

    @BeforeEach
    void setUp() {
    }

    @Nested
    @DisplayName("When the registerVote method is called")
    class WhenTheRegisterVoteMethodIsCalled {

        @Nested
        @DisplayName("With invalid parameters")
        class WithInvalidParameters {

            @Test
            @DisplayName("Must throw NullPointerException")
            void mustThrowNullPointerException() {
                MemberVoteDto dto = new MemberVoteDto();

                when(votingAgendaService.findVotingAgendaById(any()))
                        .thenReturn(new VotingAgenda());

                assertThrows(NullPointerException.class,
                        () -> voteService.registerVote(1L, 1L, dto));
            }

            @Test
            @DisplayName("Must point null value")
            void mustPointNullValue() {
                MemberVoteDto dto = new MemberVoteDto(null, "Sim");

                when(votingAgendaService.findVotingAgendaById(any()))
                        .thenReturn(buildVotingAgendaWithId());
                when(assemblySessionService.findByVotingAgendaIdAndId(any(), any()))
                        .thenReturn(buildAssemblySession());

                MemberVote result = voteService.registerVote(1L, 1L, dto);

                assertThat(result, nullValue());
            }
        }

        @Nested
        @DisplayName("With valid parameters")
        class WithValidParameters {

            @Test
            @DisplayName("Must register a vote")
            void mustRegisterAVote() {
                MemberVoteDto dto = buildMemberVoteDto();
                MemberVote memberVote = buildMemberVoteWithVote();

                when(votingAgendaService.findVotingAgendaById(any()))
                        .thenReturn(buildVotingAgendaWithId());
                when(assemblySessionService.findByVotingAgendaIdAndId(any(), any()))
                        .thenReturn(buildAssemblySession());
                when(voteRepository.findByCpfEqualsAndVotingAgendaId(any(), any()))
                        .thenReturn(null);

                voteService.registerVote(1L, 1L, dto);

                verify(voteRepository).save(memberVote);
            }
        }
    }

    @Nested
    @DisplayName("When the findAll method is called")
    class WhenTheFindAllMethodIsCalled {

        @Nested
        @DisplayName("When there is no data in the database")
        class WhenThereIsNoDataInTheDatabase {

            @Test
            @DisplayName("Must return NotFoundException")
            void mustReturnNotFoundException() {
                List<MemberVote> memberVoteList = new ArrayList<>();

                when(voteRepository.findAll())
                        .thenReturn((memberVoteList));

                assertThrows(NotFoundException.class,
                        () -> voteService.findAll());
            }
        }

        @Nested
        @DisplayName("When there is data in the database")
        class WhenThereIsDataInTheDatabase {

            @Test
            @DisplayName("Should return a list of member vote")
            void shouldReturnAListOfMemberVote() {
                List<MemberVote> memberVoteList = List.of(buildMemberVote());

                when(voteRepository.findAll())
                        .thenReturn((memberVoteList));

                List<MemberVote> result = voteService.findAll();

                assertThat(result.get(0).getVotingAgenda().getVotingAgendaTitle(), is("Title"));
            }
        }
    }

    @Nested
    @DisplayName("When the findById method is called")
    class WhenTheFindByIdMethodIsCalled {

        @Nested
        @DisplayName("When member vote is not found in the database")
        class WhenMemberVoteIsNotFoundInTheDatabase {

            @Test
            @DisplayName("Must return Exception")
            void mustReturnException() {
                when(voteRepository.findById(any()))
                        .thenThrow(VotingAgendaNotFoundException.class);

                assertThrows(VotingAgendaNotFoundException.class,
                        () -> voteService.findById(1L));
            }
        }

        @Nested
        @DisplayName("When member vote is found in the database")
        class WhenMemberVoteIsFoundInTheDatabase {

            @Test
            @DisplayName("Should return a member vote")
            void shouldReturnAVotingAgenda() {
                when(voteRepository.findById(any()))
                        .thenReturn(Optional.of(buildMemberVote()));

                Optional<MemberVote> result = voteService.findById(1L);

                assertThat(result.get().getVotingAgenda().getVotingAgendaTitle(), is("Title"));
            }
        }
    }

    @Nested
    @DisplayName("When the getVotingResult method is called")
    class WhenTheGetVotingResultMethodIsCalled {

        @Nested
        @DisplayName("When votingAgendaId is not found in the database")
        class WhenVotingAgendaIsNotFoundInTheDatabase {

            @Test
            @DisplayName("Must return VotingAgendaNotFoundException")
            void mustReturnVotingAgendaNotFoundException() {
                when(votingAgendaService.findVotingAgendaById(any()))
                        .thenThrow(VotingAgendaNotFoundException.class);

                assertThrows(VotingAgendaNotFoundException.class,
                        () -> voteService.getVotingResult(1L));
            }
        }

        @Nested
        @DisplayName("When votingAgendaId is found in the database")
        class WhenVotingAgendaIsFoundInTheDatabase {

            @Test
            @DisplayName("Must return VotingResponse")
            void mustReturnVotingResponse() {
                VotingAgenda votingAgenda = buildVotingAgendaWithId();

                when(votingAgendaService.findVotingAgendaById(any()))
                        .thenReturn(votingAgenda);

                VotingResponse result = voteService.getVotingResult(1L);

                assertThat(result.getVotingAgendaTitle(), is("Title"));
            }
        }
    }
}
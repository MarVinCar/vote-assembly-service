package com.vote.assembly.service.service.impl;

import com.vote.assembly.service.dto.AssemblySessionDto;
import com.vote.assembly.service.exception.NotFoundException;
import com.vote.assembly.service.exception.SessionNotFoundException;
import com.vote.assembly.service.model.AssemblySession;
import com.vote.assembly.service.model.VotingAgenda;
import com.vote.assembly.service.repository.AssemblySessionRepository;
import com.vote.assembly.service.service.VotingAgendaService;
import com.vote.assembly.service.util.Messages;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static fixture.AssemblySessionFixture.buildAssemblySession;
import static fixture.AssemblySessionFixture.buildAssemblySessionDto;
import static fixture.VotingAgendaFixture.buildVotingAgenda;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AssemblySessionServiceImplTest {

    @InjectMocks
    private AssemblySessionServiceImpl assemblySessionService;

    @Mock
    private AssemblySessionRepository assemblySessionRepository;

    @Mock
    private VotingAgendaService votingAgendaService;

    @Mock
    private Messages message;

    @BeforeEach
    void setUp() {
    }

    @Nested
    @DisplayName("When the startSession method is called")
    class WhenTheStartSessionMethodIsCalled {

        @Nested
        @DisplayName("With invalid parameters")
        class WithInvalidParameters {

            @Test
            @DisplayName("Must point null value")
            void mustPointNullValue() {
                AssemblySessionDto dto = new AssemblySessionDto();

                when(votingAgendaService.findVotingAgendaById(any()))
                        .thenReturn(new VotingAgenda());

                AssemblySession result = assemblySessionService.startSession(1L, dto);

                assertThat(result, nullValue());
            }
        }

        @Nested
        @DisplayName("With valid parameters")
        class WithValidParameters {

            @Test
            @DisplayName("Must create a assembly session")
            void mustCreateAAssemblySession() {
                AssemblySessionDto dto = buildAssemblySessionDto();
                VotingAgenda votingAgenda = buildVotingAgenda();
                AssemblySession assemblySession = buildAssemblySession();

                when(votingAgendaService.findVotingAgendaById(any()))
                        .thenReturn(votingAgenda);
                when(assemblySessionRepository.save(any()))
                        .thenReturn(assemblySession);

                AssemblySession result = assemblySessionService.startSession(1L, dto);

                assertThat(result.getVotingAgenda().getVotingAgendaTitle(), is("Title"));
            }
        }
    }

    @Nested
    @DisplayName("When the findAll method is called")
    class WhenTheFindAllMethodIsCalled {

        @Nested
        @DisplayName("When there is no data in the database")
        class WhenThereIsNoDataInTheDatabase {

            @Test
            @DisplayName("Must return NotFoundException")
            void mustReturnNotFoundException() {
                List<AssemblySession> assemblySessionList = new ArrayList<>();

                when(assemblySessionRepository.findAll())
                        .thenReturn((assemblySessionList));

                assertThrows(NotFoundException.class,
                        () -> assemblySessionService.findAll());
            }
        }

        @Nested
        @DisplayName("When there is data in the database")
        class WhenThereIsDataInTheDatabase {

            @Test
            @DisplayName("Should return a list of assembly session")
            void shouldReturnAListOfAssemblySession() {
                List<AssemblySession> assemblySessionList = List.of(buildAssemblySession());

                when(assemblySessionRepository.findAll())
                        .thenReturn((assemblySessionList));

                List<AssemblySession> result = assemblySessionService.findAll();

                assertThat(result.get(0).getVotingAgenda().getVotingAgendaTitle(), is("Title"));
            }
        }
    }

    @Nested
    @DisplayName("When the findById method is called")
    class WhenTheFindByIdMethodIsCalled {

        @Nested
        @DisplayName("When assembly session is not found in the database")
        class WhenAssemblySessionIsNotFoundInTheDatabase {

            @Test
            @DisplayName("Must return SessionNotFoundException")
            void mustReturnSessionNotFoundException() {
                when(assemblySessionRepository.findById(any()))
                        .thenThrow(SessionNotFoundException.class);

                assertThrows(SessionNotFoundException.class,
                        () -> assemblySessionService.findById(1L));
            }
        }

        @Nested
        @DisplayName("When assembly session is found in the database")
        class WhenAssemblySessionIsFoundInTheDatabase {

            @Test
            @DisplayName("Should return a assembly session")
            void shouldReturnAAssemblySession() {
                when(assemblySessionRepository.findById(any()))
                        .thenReturn(Optional.of(buildAssemblySession()));

                Optional<AssemblySession> result = assemblySessionService.findById(1L);

                assertThat(result.get().getVotingAgenda().getVotingAgendaTitle(), is("Title"));
            }
        }
    }

    @Nested
    @DisplayName("When the findByVotingAgendaIdAndId method is called")
    class WhenTheFindByVotingAgendaIdAndIdMethodIsCalled {

        @Nested
        @DisplayName("When assembly session is not found in the database")
        class WhenAssemblySessionIsNotFoundInTheDatabase {

            @Test
            @DisplayName("Must return SessionNotFoundException")
            void mustReturnSessionNotFoundException() {
                when(assemblySessionRepository.findByVotingAgendaIdAndId(any(), any()))
                        .thenThrow(SessionNotFoundException.class);

                assertThrows(SessionNotFoundException.class,
                        () -> assemblySessionService.findByVotingAgendaIdAndId(1L, 1L));
            }
        }

        @Nested
        @DisplayName("When assembly session is found in the database")
        class WhenAssemblySessionIsFoundInTheDatabase {

            @Test
            @DisplayName("Should return a assembly session")
            void shouldReturnAAssemblySession() {
                when(assemblySessionRepository.findByVotingAgendaIdAndId(any(), any()))
                        .thenReturn(buildAssemblySession());

                AssemblySession result = assemblySessionService.findByVotingAgendaIdAndId(1L, 1L);

                assertThat(result.getVotingAgenda().getVotingAgendaTitle(), is("Title"));
            }
        }
    }
}
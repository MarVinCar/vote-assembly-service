package fixture;

import com.vote.assembly.service.dto.AssemblySessionDto;
import com.vote.assembly.service.model.AssemblySession;

import java.time.LocalDateTime;

import static fixture.VotingAgendaFixture.buildVotingAgendaWithId;

public class AssemblySessionFixture {

    public static AssemblySessionDto buildAssemblySessionDto() {
        return new AssemblySessionDto(null, null);
    }

    public static AssemblySession buildAssemblySession() {
        long sessionDurationTime = 1L;
        LocalDateTime sessionOpeningDate = LocalDateTime.now();
        LocalDateTime sessionClosingDate = sessionOpeningDate.plusMinutes(sessionDurationTime);
        return AssemblySession.builder()
                .id(1L)
                .votingAgenda(buildVotingAgendaWithId())
                .sessionOpeningDate(sessionOpeningDate)
                .sessionDurationTime(sessionDurationTime)
                .sessionClosingDate(sessionClosingDate)
                .build();
    }
}

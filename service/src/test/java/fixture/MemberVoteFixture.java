package fixture;

import com.vote.assembly.service.dto.MemberVoteDto;
import com.vote.assembly.service.model.MemberVote;

import static fixture.VotingAgendaFixture.buildVotingAgendaWithId;
import static fixture.VotingAgendaFixture.buildVotingAgendaWithIdAndVote;

public class MemberVoteFixture {

    public static MemberVoteDto buildMemberVoteDto() {
        return MemberVoteDto.builder()
                .cpf("76674597031")
                .vote("Sim")
                .build();
    }

    public static MemberVote buildMemberVote() {
        return MemberVote.builder()
                .cpf("76674597031")
                .votingAgenda(buildVotingAgendaWithId())
                .vote(true)
                .build();
    }

    public static MemberVote buildMemberVoteWithVote() {
        return MemberVote.builder()
                .cpf("76674597031")
                .votingAgenda(buildVotingAgendaWithIdAndVote())
                .vote(true)
                .build();
    }
}

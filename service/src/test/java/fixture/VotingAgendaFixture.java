package fixture;

import com.vote.assembly.service.dto.VotingAgendaDto;
import com.vote.assembly.service.model.VotingAgenda;

public class VotingAgendaFixture {

    public static VotingAgendaDto buildVotingAgendaDto() {
        return new VotingAgendaDto("Title");
    }

    public static VotingAgenda buildVotingAgenda() {
        return new VotingAgenda(null, "Title");
    }

    public static VotingAgenda buildVotingAgendaWithId() {
        return new VotingAgenda(1L, "Title");
    }

    public static VotingAgenda buildVotingAgendaWithIdAndVote() {
        return new VotingAgenda(1L, "Title");
    }
}

package com.vote.assembly.service.service;


import com.vote.assembly.service.dto.AssemblySessionDto;
import com.vote.assembly.service.model.AssemblySession;

import java.util.List;
import java.util.Optional;

public interface AssemblySessionService {

    AssemblySession startSession(Long id, AssemblySessionDto dto);

    List<AssemblySession> findAll();

    Optional<AssemblySession> findById(Long id);

    AssemblySession findByVotingAgendaIdAndId(Long votingAgendaId, Long id);

}

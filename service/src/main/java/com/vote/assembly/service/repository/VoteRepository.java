package com.vote.assembly.service.repository;

import com.vote.assembly.service.model.MemberVote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<MemberVote, Long> {

    MemberVote findByCpfEqualsAndVotingAgendaId(String cpf, Long votingAgendaId);

    Long countByVotingAgendaIdAndVote(Long votingAgendaId, boolean vote);
}

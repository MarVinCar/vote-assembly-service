package com.vote.assembly.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SessionNotFoundException extends GeneralException {

    public SessionNotFoundException(String message) {
        super(message);
    }
}

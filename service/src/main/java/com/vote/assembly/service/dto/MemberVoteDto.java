package com.vote.assembly.service.dto;

import com.vote.assembly.service.constants.Constant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MemberVoteDto implements Serializable {
    @Serial
    private static final long serialVersionUID = -7434299383860886754L;

    @CPF(message = Constant.FIELD_CPF_INVALID)
    @NotNull(message = Constant.FIELD_CPF_NOT_NULL)
    private String cpf;

    @NotNull(message = Constant.FIELD_VOTE_NOT_NULL)
    private String vote;

}

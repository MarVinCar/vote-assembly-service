package com.vote.assembly.service.controller;

import com.vote.assembly.service.constants.Constant;
import com.vote.assembly.service.dto.VotingAgendaDto;
import com.vote.assembly.service.model.VotingAgenda;
import com.vote.assembly.service.service.VotingAgendaService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping
@Tag(name = Constant.VOTING_AGENDA_NAME)
public class VotingAgendaController {

    private final VotingAgendaService service;

    @Autowired
    public VotingAgendaController(
            VotingAgendaService service
    ) {
        this.service = service;
    }

    @PostMapping(path = "${create.voting.agenda.path}")
    @Operation(summary = Constant.CREATING_VOTING_AGENDA)
    @ResponseStatus(HttpStatus.CREATED)
    public VotingAgenda createVotingAgenda(
            @RequestBody
            @Valid VotingAgendaDto dto
    ) {
        return this.service.createVotingAgenda(dto);
    }

    @GetMapping(path = "${find.all.voting.agenda.path}")
    @Operation(summary = Constant.FIND_ALL_VOTING_AGENDA)
    @ResponseStatus(HttpStatus.OK)
    public List<VotingAgenda> findAll() {
        return this.service.findAll();
    }

    @GetMapping(path = "${find.by.id.voting.agenda.path}")
    @Operation(summary = Constant.FIND_BY_ID_VOTING_AGENDA)
    @ResponseStatus(HttpStatus.OK)
    public VotingAgenda findVotingAgendaById(
            @PathVariable Long id
    ) {
        return this.service.findVotingAgendaById(id);
    }

}

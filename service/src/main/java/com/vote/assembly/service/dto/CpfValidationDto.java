package com.vote.assembly.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CpfValidationDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 365605980515080715L;

    private String status;

    private String body;

}

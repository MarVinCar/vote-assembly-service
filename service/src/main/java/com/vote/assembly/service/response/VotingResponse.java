package com.vote.assembly.service.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VotingResponse {

    private Long votingAgendaId;

    private String votingAgendaTitle;

    private String votingAgendaResult;

    private Long totalVotes;

    private Long votesYes;

    private Long votesNo;

    public static Long totalVotes(Long votesYes, Long votesNo) {
        return votesYes + votesNo;
    }

}

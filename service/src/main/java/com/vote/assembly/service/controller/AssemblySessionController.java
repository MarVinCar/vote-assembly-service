package com.vote.assembly.service.controller;

import com.vote.assembly.service.constants.Constant;
import com.vote.assembly.service.dto.AssemblySessionDto;
import com.vote.assembly.service.model.AssemblySession;
import com.vote.assembly.service.service.AssemblySessionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
@RequestMapping
@Tag(name = Constant.SESSION_NAME)
public class AssemblySessionController {

    private final AssemblySessionService sessaoService;

    @Autowired
    public AssemblySessionController(
            AssemblySessionService sessaoService
    ) {
        this.sessaoService = sessaoService;
    }

    @PostMapping(path = "${start.session.path}")
    @Operation(summary = Constant.START_SESSION)
    @ResponseStatus(HttpStatus.CREATED)
    public AssemblySession startSession(
            @PathVariable
            Long votingAgendaId,
            @RequestBody
            @Valid AssemblySessionDto dto
    ) {
        return this.sessaoService.startSession(votingAgendaId, dto);
    }

    @GetMapping(path = "${find.all.session.path}")
    @Operation(summary = Constant.FIND_ALL_SESSION)
    @ResponseStatus(HttpStatus.OK)
    public List<AssemblySession> findAll() {
        return this.sessaoService.findAll();
    }

    @GetMapping(path = "${find.by.id.session.path}")
    @Operation(summary = Constant.FIND_BY_ID_SESSION)
    @ResponseStatus(HttpStatus.OK)
    public Optional<AssemblySession> findById(
            @PathVariable Long id
    ) {
        return this.sessaoService.findById(id);
    }

}

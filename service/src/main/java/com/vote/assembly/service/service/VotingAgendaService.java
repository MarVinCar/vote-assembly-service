package com.vote.assembly.service.service;

import com.vote.assembly.service.dto.VotingAgendaDto;
import com.vote.assembly.service.model.VotingAgenda;

import java.util.List;

public interface VotingAgendaService {

    VotingAgenda createVotingAgenda(VotingAgendaDto dto);

    List<VotingAgenda> findAll();

    VotingAgenda findVotingAgendaById(Long id);

}

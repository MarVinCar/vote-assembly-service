package com.vote.assembly.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AssemblySessionDto implements Serializable {
    @Serial
    private static final long serialVersionUID = -209273947332373163L;

    private LocalDateTime sessionOpeningDate;

    private Long sessionDurationTime;


}


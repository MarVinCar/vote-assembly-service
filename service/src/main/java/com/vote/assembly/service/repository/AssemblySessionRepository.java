package com.vote.assembly.service.repository;

import com.vote.assembly.service.model.AssemblySession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssemblySessionRepository extends JpaRepository<AssemblySession, Long> {

    AssemblySession findByVotingAgendaIdAndId(Long votingAgendaId, Long id);

}


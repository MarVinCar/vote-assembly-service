package com.vote.assembly.service.service.impl;

import com.vote.assembly.service.constants.Constant;
import com.vote.assembly.service.dto.CpfValidationDto;
import com.vote.assembly.service.dto.MemberVoteDto;
import com.vote.assembly.service.enums.Status;
import com.vote.assembly.service.exception.*;
import com.vote.assembly.service.feign.ClientValidateCpf;
import com.vote.assembly.service.model.AssemblySession;
import com.vote.assembly.service.model.MemberVote;
import com.vote.assembly.service.model.VotingAgenda;
import com.vote.assembly.service.repository.VoteRepository;
import com.vote.assembly.service.response.VotingResponse;
import com.vote.assembly.service.service.AssemblySessionService;
import com.vote.assembly.service.service.VoteService;
import com.vote.assembly.service.service.VotingAgendaService;
import com.vote.assembly.service.util.Messages;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.vote.assembly.service.enums.MessageEnum.*;

@Service
@Slf4j
public class VoteServiceImpl implements VoteService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VotingAgendaServiceImpl.class);
    private final AssemblySessionService assemblySessionService;
    private final ClientValidateCpf client;
    private final Messages messages;
    private final VoteRepository voteRepository;
    private final VotingAgendaService votingAgendaService;

    @Autowired
    public VoteServiceImpl(
            Messages messages,
            VoteRepository voteRepository,
            VotingAgendaService votingAgendaService,
            AssemblySessionService assemblySessionService,
            ClientValidateCpf client
    ) {
        this.messages = messages;
        this.voteRepository = voteRepository;
        this.votingAgendaService = votingAgendaService;
        this.assemblySessionService = assemblySessionService;
        this.client = client;
    }

    @Override
    public MemberVote registerVote(Long votingAgendaId, Long sessionId, MemberVoteDto dto) {
        LOGGER.info("votingAgendaId: {} sessionId: {} dto: {}", votingAgendaId, sessionId, dto);
        String cpf = dto.getCpf();
        VotingAgenda votingAgenda = this.votingAgendaService.findVotingAgendaById(votingAgendaId);

        isVoted(cpf, votingAgendaId);

//        TODO: Validar url 'https://user-info.herokuapp.com' para descomentar.
//        validateStatusCpfToVote(cpf);

        Boolean vote = validateVotingOption(dto);

        validateAssemblySessionPeriod(votingAgendaId, sessionId);

        MemberVote memberVote = MemberVote.builder()
                .cpf(cpf)
                .votingAgenda(votingAgenda)
                .vote(vote)
                .build();
        LOGGER.info("memberVote: {}", memberVote);
        return this.voteRepository.save(memberVote);
    }

    @Override
    public List<MemberVote> findAll() {
        List<MemberVote> vote = this.voteRepository.findAll();

        if (vote.isEmpty()) {
            throw new NotFoundException(messages.getMessage(EXCEPTION_MEMBER_VOTE_NOT_FOUND_ALL.getKey()));
        }

        return vote;
    }

    @Override
    public Optional<MemberVote> findById(Long id) {
        Optional<MemberVote> vote = this.voteRepository.findById(id);
        LOGGER.info("vote: {}", vote);

        if (vote.isEmpty()) {
            throw new MemberVoteNotFoundException(messages.getMessage(EXCEPTION_MEMBER_VOTE_NOT_FOUND.getKey(), id));
        }

        return vote;
    }

    @Override
    public VotingResponse getVotingResult(Long votingAgendaId) {
        VotingAgenda votingAgenda = this.votingAgendaService.findVotingAgendaById(votingAgendaId);
        LOGGER.info("votingAgenda: {}", votingAgenda);
        Long votesYes = this.voteRepository.countByVotingAgendaIdAndVote(votingAgendaId, true);
        Long votesNo = this.voteRepository.countByVotingAgendaIdAndVote(votingAgendaId, false);

        return VotingResponse.builder()
                .votingAgendaId(votingAgendaId)
                .votingAgendaTitle(votingAgenda.getVotingAgendaTitle())
                .votingAgendaResult(buildResultMessage(votesYes, votesNo))
                .votesYes(votesYes)
                .votesNo(votesNo)
                .totalVotes(VotingResponse.totalVotes(votesYes, votesNo))
                .build();
    }

    private void validateAssemblySessionPeriod(Long votingAgendaId, Long sessionId) {
        AssemblySession session = this.assemblySessionService.findByVotingAgendaIdAndId(votingAgendaId, sessionId);
        LocalDateTime now = LocalDateTime.now();

        if (now.isBefore(session.getSessionOpeningDate())) {
            throw new NotOpeningAssemblySessionException(messages.getMessage(EXCEPTION_NOT_OPENING_ASSEMBLY_SESSION.getKey()));
        }

        if (session.getSessionClosingDate().isBefore(now)) {
            throw new ClosedAssemblySessionException(messages.getMessage(EXCEPTION_CLOSED_ASSEMBLY_SESSION.getKey()));
        }
    }

    private void isVoted(String cpf, Long votingAgendaId) {
        MemberVote memberVote = this.voteRepository.findByCpfEqualsAndVotingAgendaId(cpf, votingAgendaId);

        if (memberVote != null) {
            throw new AlreadyVotedException(messages.getMessage(EXCEPTION_ALREADY_VOTED.getKey(), cpf));
        }
    }

    private String buildResultMessage(Long votesYes, Long votesNo) {
        String result;

        if (votesYes > votesNo) {
            result = Constant.APPROVED;
        } else if (votesYes < votesNo) {
            result = Constant.DISAPPROVED;
        } else {
            result = Constant.TIE;
        }

        return result;
    }

    private Boolean validateVotingOption(MemberVoteDto dto) {
        boolean vote;

        if (dto.getVote().equalsIgnoreCase(Constant.YES)) {
            vote = true;
        } else if (dto.getVote().equalsIgnoreCase(Constant.NO)) {
            vote = false;
        } else {
            throw new InvalidVotingOptionException(messages.getMessage(EXCEPTION_INVALID_VOTING_OPTION.getKey()));
        }

        return vote;
    }

    private ResponseEntity<CpfValidationDto> validateStatusCpfToVote(String cpf) {
        ResponseEntity<CpfValidationDto> cpfValidate = this.client.validateCpf(cpf);

        if (cpfValidate.getStatusCode().equals(HttpStatus.OK)) {
            if (cpfValidate.getBody().equals(Status.ENABLE_TO_VOTE)) {
                throw new MemberNotEligibleToVoteException(messages.getMessage(EXCEPTION_MEMBER_NOT_ELIGIBLE_TO_VOTE.getKey()));
            }
        } else {
            throw new InvalidCpfException(messages.getMessage(EXCEPTION_INVALID_CPF.getKey(), cpf));
        }
        return cpfValidate;
    }

}

package com.vote.assembly.service.service;

import com.vote.assembly.service.dto.MemberVoteDto;
import com.vote.assembly.service.model.MemberVote;
import com.vote.assembly.service.response.VotingResponse;

import java.util.List;
import java.util.Optional;

public interface VoteService {

    MemberVote registerVote(Long votingAgendaId, Long sessionId, MemberVoteDto dto);

    List<MemberVote> findAll();

    Optional<MemberVote> findById(Long id);

    VotingResponse getVotingResult(Long votingAgendaId);

}

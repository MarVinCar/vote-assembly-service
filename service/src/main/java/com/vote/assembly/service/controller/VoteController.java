package com.vote.assembly.service.controller;

import com.vote.assembly.service.constants.Constant;
import com.vote.assembly.service.dto.MemberVoteDto;
import com.vote.assembly.service.model.MemberVote;
import com.vote.assembly.service.response.VotingResponse;
import com.vote.assembly.service.service.VoteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
@RequestMapping
@Tag(name = Constant.VOTE_NAME)
public class VoteController {

    private final VoteService voteService;

    @Autowired
    public VoteController(VoteService voteService
    ) {
        this.voteService = voteService;
    }

    @PostMapping(path = "${register.vote.path}")
    @Operation(summary = Constant.REGISTER_VOTE)
    @ResponseStatus(HttpStatus.CREATED)
    public MemberVote registerVote(
            @PathVariable
            Long votingAgendaId,
            @PathVariable
            Long sessionId,
            @RequestBody
            @Valid MemberVoteDto dto
    ) {
        return this.voteService.registerVote(votingAgendaId, sessionId, dto);
    }

    @GetMapping(path = "${find.all.vote.path}")
    @Operation(summary = Constant.FIND_ALL_VOTE)
    @ResponseStatus(HttpStatus.OK)
    public List<MemberVote> findAll() {
        return this.voteService.findAll();
    }

    @GetMapping(path = "${find.by.id.vote.path}")
    @Operation(summary = Constant.FIND_BY_ID_VOTE)
    @ResponseStatus(HttpStatus.OK)
    public Optional<MemberVote> findById(
            @PathVariable Long id
    ) {
        return this.voteService.findById(id);
    }

    @GetMapping(path = "${get.voting.result.path}")
    @Operation(summary = Constant.GET_VOTING_RESULT)
    @ResponseStatus(HttpStatus.OK)
    public VotingResponse getVotingResult(
            @PathVariable Long votingAgendaId
    ) {
        return this.voteService.getVotingResult(votingAgendaId);
    }

}

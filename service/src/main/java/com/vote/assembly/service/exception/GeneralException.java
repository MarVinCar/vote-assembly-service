package com.vote.assembly.service.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GeneralException extends RuntimeException {

    private String message;

    public GeneralException(String message) {
        super(message);
        this.message = message;
    }
}

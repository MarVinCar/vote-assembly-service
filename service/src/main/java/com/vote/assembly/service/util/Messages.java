package com.vote.assembly.service.util;

import java.util.Locale;

public interface Messages {

    String getMessage(String key);

    String getMessage(String key, Object... attrs);

    String getMessage(String key, Locale locale);

}

package com.vote.assembly.service.enums;

import lombok.Getter;

@Getter
public enum MessageEnum {

    EXCEPTION_ALREADY_VOTED                     ("exception.already.voted"),
    EXCEPTION_CLOSED_ASSEMBLY_SESSION           ("exception.closed.assembly.session"),
    EXCEPTION_INVALID_CPF                       ("exception.invalid.cpf"),
    EXCEPTION_INVALID_VOTING_OPTION             ("exception.invalid.voting.option"),
    EXCEPTION_MEMBER_NOT_ELIGIBLE_TO_VOTE       ("exception.member.not.eligible.to.vote"),
    EXCEPTION_MEMBER_VOTE_NOT_FOUND             ("exception.member.vote.not.found"),
    EXCEPTION_MEMBER_VOTE_NOT_FOUND_ALL         ("exception.member.vote.not.found.all"),
    EXCEPTION_NOT_OPENING_ASSEMBLY_SESSION      ("exception.not.opening.assembly.session"),
    EXCEPTION_SESSION_NOT_FOUND_ID              ("exception.session.not.found.id"),
    EXCEPTION_SESSION_NOT_FOUND_ALL             ("exception.session.not.found.all"),
    EXCEPTION_SESSION_NOT_FOUND                 ("exception.session.not.found"),
    EXCEPTION_VOTING_AGENDA_NOT_FOUND           ("exception.voting.agenda.not.found"),
    EXCEPTION_VOTING_AGENDA_NOT_FOUND_ALL       ("exception.voting.agenda.not.found.all");

    private final String key;

    MessageEnum(String key) {
        this.key = key;
    }
}

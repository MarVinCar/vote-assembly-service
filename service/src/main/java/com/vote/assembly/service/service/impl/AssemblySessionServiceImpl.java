package com.vote.assembly.service.service.impl;

import com.vote.assembly.service.dto.AssemblySessionDto;
import com.vote.assembly.service.exception.NotFoundException;
import com.vote.assembly.service.exception.SessionNotFoundException;
import com.vote.assembly.service.model.AssemblySession;
import com.vote.assembly.service.model.VotingAgenda;
import com.vote.assembly.service.repository.AssemblySessionRepository;
import com.vote.assembly.service.service.AssemblySessionService;
import com.vote.assembly.service.service.VotingAgendaService;
import com.vote.assembly.service.util.Messages;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.vote.assembly.service.enums.MessageEnum.*;

@Service
@Slf4j
public class AssemblySessionServiceImpl implements AssemblySessionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VotingAgendaServiceImpl.class);
    private final AssemblySessionRepository assemblySessionRepository;
    private final Messages messages;
    private final VotingAgendaService votingAgendaService;

    @Autowired
    public AssemblySessionServiceImpl(
            AssemblySessionRepository assemblySessionRepository,
            Messages messages,
            VotingAgendaService votingAgendaService
    ) {
        this.assemblySessionRepository = assemblySessionRepository;
        this.messages = messages;
        this.votingAgendaService = votingAgendaService;
    }

    @Override
    public AssemblySession startSession(Long votingAgendaId, AssemblySessionDto dto) {
        LOGGER.info("votingAgendaId: {} dto: {}", votingAgendaId, dto);
        VotingAgenda votingAgenda = this.votingAgendaService.findVotingAgendaById(votingAgendaId);
        LocalDateTime sessionOpeningDate = dto.getSessionOpeningDate();
        Long sessionDurationTime = dto.getSessionDurationTime();

        sessionOpeningDate = setSessionOpeningDate(sessionOpeningDate);

        sessionDurationTime = setSessionDurationTime(sessionDurationTime);

        LocalDateTime sessionClosingDate = setSessionClosingDate(sessionOpeningDate, sessionDurationTime);

        AssemblySession assemblySession = AssemblySession.builder()
                .sessionOpeningDate(sessionOpeningDate)
                .votingAgenda(votingAgenda)
                .sessionDurationTime(sessionDurationTime)
                .sessionClosingDate(sessionClosingDate)
                .build();
        LOGGER.info("AssemblySession: {}", assemblySession);
        return this.assemblySessionRepository.save(assemblySession);
    }

    @Override
    public List<AssemblySession> findAll() {
        List<AssemblySession> session = this.assemblySessionRepository.findAll();

        if (session.isEmpty()) {
            throw new NotFoundException(messages.getMessage(EXCEPTION_SESSION_NOT_FOUND_ALL.getKey()));
        }

        return session;
    }

    @Override
    public Optional<AssemblySession> findById(Long id) {
        Optional<AssemblySession> session = this.assemblySessionRepository.findById(id);
        LOGGER.info("session: {}", session);

        if (session.isEmpty()) {
            throw new SessionNotFoundException(messages.getMessage(EXCEPTION_SESSION_NOT_FOUND_ID.getKey(), id));
        }

        return session;
    }

    @Override
    public AssemblySession findByVotingAgendaIdAndId(Long votingAgendaId, Long id) {
        AssemblySession session = this.assemblySessionRepository.findByVotingAgendaIdAndId(votingAgendaId, id);
        LOGGER.info("votingAgendaId: {} sessionId: {}", votingAgendaId, id);

        if (session == null) {
            throw new SessionNotFoundException(messages.getMessage(EXCEPTION_SESSION_NOT_FOUND.getKey()));
        }

        return session;
    }

    private LocalDateTime setSessionOpeningDate(LocalDateTime sessionOpeningDate) {
        if (sessionOpeningDate == null) {
            sessionOpeningDate = LocalDateTime.now();
        }
        return sessionOpeningDate;
    }

    private Long setSessionDurationTime(Long sessionDurationTime) {
        if (sessionDurationTime == null || sessionDurationTime <= 0) {
            sessionDurationTime = 1L;
        }
        return sessionDurationTime;
    }

    private LocalDateTime setSessionClosingDate(LocalDateTime sessionOpeningDate, Long sessionDurationTime) {
        return sessionOpeningDate.plusMinutes(sessionDurationTime);
    }

}

package com.vote.assembly.service.feign;

import com.vote.assembly.service.constants.Constant;
import com.vote.assembly.service.dto.CpfValidationDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(url = "${client.validete.cpf.path}", name = Constant.CLIENT_VALIDATE_CPF_NAME)
public interface ClientValidateCpf {

    @GetMapping(path = "${client.user.path}")
    ResponseEntity<CpfValidationDto> validateCpf(@PathVariable(Constant.CPF) String cpf);

}

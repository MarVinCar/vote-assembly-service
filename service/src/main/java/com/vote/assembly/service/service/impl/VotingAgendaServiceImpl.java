package com.vote.assembly.service.service.impl;

import com.vote.assembly.service.dto.VotingAgendaDto;
import com.vote.assembly.service.exception.NotFoundException;
import com.vote.assembly.service.exception.VotingAgendaNotFoundException;
import com.vote.assembly.service.model.VotingAgenda;
import com.vote.assembly.service.repository.VotingAgendaRepository;
import com.vote.assembly.service.service.VotingAgendaService;
import com.vote.assembly.service.util.Messages;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.vote.assembly.service.enums.MessageEnum.EXCEPTION_VOTING_AGENDA_NOT_FOUND;
import static com.vote.assembly.service.enums.MessageEnum.EXCEPTION_VOTING_AGENDA_NOT_FOUND_ALL;

@Service
@Slf4j
public class VotingAgendaServiceImpl implements VotingAgendaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(VotingAgendaServiceImpl.class);
    private final VotingAgendaRepository votingAgendaRepository;
    private final Messages messages;

    public VotingAgendaServiceImpl(
            VotingAgendaRepository votingAgendaRepository,
            Messages messages
    ) {
        this.votingAgendaRepository = votingAgendaRepository;
        this.messages = messages;
    }

    @Override
    public VotingAgenda createVotingAgenda(VotingAgendaDto dto) {
        LOGGER.info("votingAgendaTitle: {}", dto.getVotingAgendaTitle());
        VotingAgenda votingAgenda = VotingAgenda.builder()
                .votingAgendaTitle(dto.getVotingAgendaTitle())
                .build();
        LOGGER.info("votingAgenda: {}", votingAgenda);
        return this.votingAgendaRepository.save(votingAgenda);
    }

    @Override
    public List<VotingAgenda> findAll() {
        List<VotingAgenda> votingAgenda = this.votingAgendaRepository.findAll();

        if (votingAgenda.isEmpty()) {
            throw new NotFoundException(messages.getMessage(EXCEPTION_VOTING_AGENDA_NOT_FOUND_ALL.getKey()));
        }

        return votingAgenda;
    }

    @Override
    public VotingAgenda findVotingAgendaById(Long id) {
        VotingAgenda votingAgenda = this.votingAgendaRepository.findVotingAgendaById(id);
        LOGGER.info("votingAgenda: {}", votingAgenda);

        if (votingAgenda == null) {
            throw new VotingAgendaNotFoundException(messages.getMessage(EXCEPTION_VOTING_AGENDA_NOT_FOUND.getKey(), id));
        }

        return votingAgenda;
    }

}

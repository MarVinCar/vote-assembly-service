package com.vote.assembly.service.enums;

public enum Status {

    ABLE_TO_VOTE,
    ENABLE_TO_VOTE;

}

package com.vote.assembly.service.model;

import lombok.*;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ErrorWrapper {
    private List<Error> errors;

    public ErrorWrapper(Error error) {
        this.errors = List.of(error);
    }
}

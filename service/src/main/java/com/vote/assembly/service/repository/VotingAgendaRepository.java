package com.vote.assembly.service.repository;

import com.vote.assembly.service.model.VotingAgenda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotingAgendaRepository extends JpaRepository<VotingAgenda, Long> {

    VotingAgenda findVotingAgendaById(Long votingAgendaId);

}

package com.vote.assembly.service.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class OpenApiConfig {

    @Bean
    public OpenAPI customOpenApi() {
        return new OpenAPI()
                .components(new Components())
                .info(new Info()
                        .title("Vote Assembly Service API")
                        .description("This is a sample Vote Assembly Service based on the OpenAPI 3.0 specification")
                        .version("1.0")
                );
    }
}

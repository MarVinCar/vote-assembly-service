package com.vote.assembly.service.constants;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Constant {

    /*
     * SESSION
     */
    public static final String SESSION_NAME = "Sessões de Assembléia de Votação";
    public static final String START_SESSION = "Abrir uma nova Sessão de Votação";
    public static final String FIND_ALL_SESSION = "Buscar todas as Sessões de Votação";
    public static final String FIND_BY_ID_SESSION = "Buscar uma Sessão de Votação por id";

    /*
     * VOTE
     */
    public static final String VOTE_NAME = "Voto do Associado";
    public static final String REGISTER_VOTE = "Registrar um Voto de Associado";
    public static final String FIND_ALL_VOTE = "Buscar todos os Votos de Associados";
    public static final String FIND_BY_ID_VOTE = "Buscar um Voto de Associado por id";
    public static final String GET_VOTING_RESULT = "Busca Resultado de uma Votação por id da Pauta";

    /*
     * VOTING_AGENDA
     */
    public static final String VOTING_AGENDA_NAME = "Pautas de Votação";
    public static final String CREATING_VOTING_AGENDA = "Criar uma nova Pauta de Votação";
    public static final String FIND_ALL_VOTING_AGENDA = "Buscar todas as Pautas de Votação";
    public static final String FIND_BY_ID_VOTING_AGENDA = "Buscar uma Pauta de Votação por id";
    public static final String APPROVED = "Esta Pauta está Aprovada por maioria de votos.";
    public static final String DISAPPROVED = "Esta Pauta está Reprovada por maioria de votos.";
    public static final String TIE = "Esta Pauta está empatada.";

    /*
     * VALIDATION
     */
    public static final String FIELD_CPF_NOT_NULL = "O campo 'CPF' não pode ser nulo ou vazio.";
    public static final String FIELD_VOTE_NOT_NULL = "O campo 'Voto' não pode ser nulo ou vazio.";
    public static final String FIELD_VOTING_AGENDA_TITLE_NOT_NULL = "O campo 'Título da Pauta' não pode ser nulo ou vazio.";
    public static final String FIELD_CPF_INVALID = "CPF inválido.";
    public static final String CLIENT_VALIDATE_CPF_NAME = "ClientValidateCpf";

    /*
     * FORMAT
     */
    public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json;charset=UTF-8";
    public static final String CPF = "cpf";
    public static final String YES = "Sim";
    public static final String NO = "Não";

}

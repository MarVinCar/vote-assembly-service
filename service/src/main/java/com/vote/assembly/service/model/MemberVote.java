package com.vote.assembly.service.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@Table(name = "vote")
@NoArgsConstructor
@AllArgsConstructor
public class MemberVote {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String cpf;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "voting_agenda_id")
    private VotingAgenda votingAgenda;

    private Boolean vote;

}
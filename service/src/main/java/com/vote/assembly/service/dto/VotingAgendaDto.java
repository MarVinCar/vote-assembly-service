package com.vote.assembly.service.dto;

import com.vote.assembly.service.constants.Constant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VotingAgendaDto implements Serializable {
    @Serial
    private static final long serialVersionUID = -7408768307144024445L;

    @NotNull(message = Constant.FIELD_VOTING_AGENDA_TITLE_NOT_NULL)
    private String votingAgendaTitle;

}

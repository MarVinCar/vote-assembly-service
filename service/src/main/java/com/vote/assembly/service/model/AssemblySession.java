package com.vote.assembly.service.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@Table(name = "session")
@NoArgsConstructor
@AllArgsConstructor
public class AssemblySession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private LocalDateTime sessionOpeningDate;

    private Long sessionDurationTime;

    private LocalDateTime sessionClosingDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "voting_agenda_id")
    private VotingAgenda votingAgenda;

}

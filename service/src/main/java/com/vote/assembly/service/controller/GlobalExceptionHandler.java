package com.vote.assembly.service.controller;

import com.vote.assembly.service.constants.Constant;
import com.vote.assembly.service.model.Error;
import com.vote.assembly.service.model.ErrorWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorWrapper> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;

        List<Error> errors = e
                .getAllErrors()
                .stream()
                .map(error -> {
                    Error treatedError = new Error();
                    treatedError.setHttpStatus(String.valueOf(badRequest.value()));
                    treatedError.setType("MethodArgumentNotValidException");
                    treatedError.setMessage(error.getDefaultMessage());
                    treatedError.setCode(badRequest.getReasonPhrase());
                    return treatedError;
                }).toList();

        log.error("Erro de validação: {}", e.getMessage());
        return ResponseEntity.status(badRequest)
                .header(HttpHeaders.CONTENT_TYPE, Constant.APPLICATION_JSON_CHARSET_UTF_8)
                .body(new ErrorWrapper(errors));
    }

}

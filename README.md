
# Desafio Assembléia de Votação

## Objetivo

No cooperativismo, cada associado possui um voto e as decisões são tomadas em assembleias, por votação. Imagine que você deve criar uma solução para dispositivos móveis para gerenciar e participar dessas sessões de votação.

Essa solução deve ser executada na nuvem e promover as seguintes funcionalidades através de uma API REST:

    • Cadastrar uma nova pauta
    • Abrir uma sessão de votação em uma pauta (a sessão de votação deve ficar aberta por um tempo determinado na chamada de abertura ou 1 minuto por default)
    • Receber votos dos associados em pautas (os votos são apenas 'Sim'/'Não'. Cada associado é identificado por um id único e pode votar apenas uma vez por pauta)
    • Contabilizar os votos e dar o resultado da votação na pauta

Para fins de exercício, a segurança das interfaces pode ser abstraída e qualquer chamada para as interfaces pode ser considerada como autorizada. A solução deve ser construída em java, usando Spring-boot, mas os frameworks e bibliotecas são de livre escolha (desde que não infrinja direitos de uso).

É importante que as pautas e os votos sejam persistidos e que não sejam perdidos com o restart da aplicação.

O foco dessa avaliação é a comunicação entre o backend e o aplicativo mobile. Essa comunicação é feita através de mensagens no formato JSON, onde essas mensagens serão interpretadas pelo cliente para montar as telas onde o usuário vai interagir com o sistema. A aplicação cliente não faz parte da avaliação, apenas os componentes do servidor. O formato padrão dessas mensagens será detalhado no anexo 1.

# Vote Assembly Service

## Dependências e ferramentas

Este projeto usa as seguintes dependências e ferramentas:

- [JDK 17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
- [Maven 3](https://archive.apache.org/dist/maven/maven-3/3.8.6/binaries/)
- [Spring Boot 2.7.8](https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.7.8&packaging=jar&jvmVersion=17&groupId=%20br.com.viavarejo&artifactId=api.front.importer.v4&name=Api%20front%20importer%20v4&description=Api%20importa%C3%A7%C3%A3o%20versao%204&packageName=%20br.com.viavarejo.api.front.importer.v4&dependencies=lombok,web,validation,cloud-feign,actuator,prometheus)
- [Spring Validation](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation)
- [Springdoc openapi](https://springdoc.org/)
- [Spring test](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-test)
- [Lombok](https://projectlombok.org/setup/maven)
- [Postgresql](https://jdbc.postgresql.org)
- [Liquibase](https://github.com/liquibase/liquibase-hibernate/wiki)
- [Docker](https://www.docker.com/)

## Executar o Projeto
#### Para rodar o projeto é necessário subir os containers que executam Postgres, PgAdmin e Liquibase:
```
docker compose up -d
```

## Link's importantes
#### Já com o projeto em execução, os link's abaixo facilitam o acesso a Documentação do projeto e Interface Gráfica do banco de dados:

SERVICE       | URL                                                                                  |
---------     |--------------------------------------------------------------------------------------|
OpenApi                 | [Swagger-ui](http://localhost:8080/vote-assembly-service/v1/swagger-ui/index.html#/) |
Client PostgresSQL      | [PgAdmin](http://localhost:16543/login?next=%2F)    |

## Desenvolvimento:

* **Marcus Vinicius Santos Cardoso**
